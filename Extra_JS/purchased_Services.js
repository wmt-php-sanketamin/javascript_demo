const _ = require("lodash")
const json1 = require("./example.json");

const arr1 = _.cloneDeep(json1.data.purchased_services);
const arr2 = _.cloneDeep(json1.data.purchased_services);

// For purchased Services

    const add1 = arr1.map((service) => {
        let filter_ser = service.purchased_office_template.purchased_office_services.filter((checkserv) => checkserv.service_selected);
        service.purchased_office_template.purchased_office_services = filter_ser.slice();
        return service;
    })
    .filter((service) => 
        service.purchased_office_template.purchased_office_services.length
    );

    console.log("Purchased Services:-   ");
    console.log(add1);

// For Additional Services

    const  add2 = arr2.map((add_service) => {
        let filter_ser = add_service.purchased_office_template.purchased_office_services.filter((checkserv) => !(checkserv.service_selected));
        add_service.purchased_office_template.purchased_office_services = filter_ser.slice();
        return add_service;
    })
    .filter((add_service) => 
        add_service.purchased_office_template.purchased_office_services.length
    )
    console.log("Additional Services");
    console.log(add2);